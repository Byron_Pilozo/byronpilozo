/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     27/11/2021 10:24:29 p. m.                    */
/*==============================================================*/


drop table if exists CARGO;

drop table if exists CLIENTE;

drop table if exists DESCUENTO;

drop table if exists EMPLEADO;

drop table if exists FACTURA;

drop table if exists FACTURA_SERVICIO;

drop table if exists FACTURA_VANDALISMO;

drop table if exists HABITACION;

drop table if exists HABITACION_TIPO;

drop table if exists HOTEL;

drop table if exists MANTENIMIENTO;

drop table if exists MANTENIMIENTO_TIPO;

drop table if exists SERVICIO;

drop table if exists VANDALISMO;

/*==============================================================*/
/* Table: CARGO                                                 */
/*==============================================================*/
create table CARGO
(
   CARGO_ID             int not null,
   CARGO_DESCRIPCION    varchar(100) not null,
   primary key (CARGO_ID)
);

/*==============================================================*/
/* Table: CLIENTE                                               */
/*==============================================================*/
create table CLIENTE
(
   CLIENTE_ID           int not null,
   CLIENTE_NOMBRE       varchar(100) not null,
   CLIENTE_APELLIDO     varchar(100) not null,
   CLIENTE_TELEFONO     int not null,
   CLIENTE_CORREO       varchar(100) not null,
   primary key (CLIENTE_ID)
);

/*==============================================================*/
/* Table: DESCUENTO                                             */
/*==============================================================*/
create table DESCUENTO
(
   DESCUENTO_ID         int not null,
   DESCUENTO_DESCRIPCION longtext not null,
   DESCUENTO_VALOR      decimal not null,
   primary key (DESCUENTO_ID)
);

/*==============================================================*/
/* Table: EMPLEADO                                              */
/*==============================================================*/
create table EMPLEADO
(
   EMPLEADO_ID          int not null,
   HOTEL_ID             int,
   CARGO_ID             int,
   EMPLEADO_NOMBRE      varchar(100) not null,
   EMPLEADO_APELLIDO    varchar(100) not null,
   EMPLEADO_TELEFONO    int not null,
   EMPLEADO_CORREO      varchar(100) not null,
   primary key (EMPLEADO_ID)
);

/*==============================================================*/
/* Table: FACTURA                                               */
/*==============================================================*/
create table FACTURA
(
   FACTURA_ID           int not null,
   HABITACION_ID        int,
   CLIENTE_ID           int,
   DESCUENTO_ID         int,
   HOTEL_ID             int,
   FACTURA_FECHA        date not null,
   FACTURA_SUBTOTAL     decimal not null,
   FACTURA_IMPUESTO     decimal not null,
   FACTURA_TOTAL        decimal not null,
   FACTURA_FECHAFIN     date not null,
   primary key (FACTURA_ID)
);

/*==============================================================*/
/* Table: FACTURA_SERVICIO                                      */
/*==============================================================*/
create table FACTURA_SERVICIO
(
   SERVICIO_ID          int not null,
   FACTURA_ID           int not null,
   FACSERV_CANTIDAD     decimal not null,
   FACSERV_TOTAL        decimal not null,
   primary key (SERVICIO_ID, FACTURA_ID)
);

/*==============================================================*/
/* Table: FACTURA_VANDALISMO                                    */
/*==============================================================*/
create table FACTURA_VANDALISMO
(
   VANDALISMO_ID        int not null,
   FACTURA_ID           int not null,
   VANDALISMO_CANTIDAD  decimal not null,
   VANDALISMO_TOTAL     decimal not null,
   primary key (VANDALISMO_ID, FACTURA_ID)
);

/*==============================================================*/
/* Table: HABITACION                                            */
/*==============================================================*/
create table HABITACION
(
   HABITACION_ID        int not null,
   HOTEL_ID             int,
   HT_ID                int,
   HABITACION_NUMERO    int not null,
   HABITACION_LOCACION  varchar(100) not null,
   HABITACION_PRECIO    decimal not null,
   primary key (HABITACION_ID)
);

/*==============================================================*/
/* Table: HABITACION_TIPO                                       */
/*==============================================================*/
create table HABITACION_TIPO
(
   HT_ID                int not null,
   HT_DESCRIPCION       longtext not null,
   primary key (HT_ID)
);

/*==============================================================*/
/* Table: HOTEL                                                 */
/*==============================================================*/
create table HOTEL
(
   HOTEL_ID             int not null,
   HOTEL_NOMBRE         varchar(100) not null,
   HOTEL_TELEFONO       varchar(100) not null,
   HOTEL_DIRECCION      varchar(100) not null,
   HOTEL_CORREO         varchar(100) not null,
   primary key (HOTEL_ID)
);

/*==============================================================*/
/* Table: MANTENIMIENTO                                         */
/*==============================================================*/
create table MANTENIMIENTO
(
   MANTENIMIENTO_ID     int not null,
   MT_ID                int,
   HABITACION_ID        int,
   EMPLEADO_ID          int,
   MANTENIMIENTO_FECHA  date not null,
   primary key (MANTENIMIENTO_ID)
);

/*==============================================================*/
/* Table: MANTENIMIENTO_TIPO                                    */
/*==============================================================*/
create table MANTENIMIENTO_TIPO
(
   MT_ID                int not null,
   MT_DESCRIPCION       varchar(100) not null,
   primary key (MT_ID)
);

/*==============================================================*/
/* Table: SERVICIO                                              */
/*==============================================================*/
create table SERVICIO
(
   SERVICIO_ID          int not null,
   EMPLEADO_ID          int,
   SERVICIO_DESCRIPCION varchar(500) not null,
   SERVICIO_COSTO       decimal not null,
   primary key (SERVICIO_ID)
);

/*==============================================================*/
/* Table: VANDALISMO                                            */
/*==============================================================*/
create table VANDALISMO
(
   VANDALISMO_ID        int not null,
   VANDALISMO_DESCRIPCION varchar(100) not null,
   VANDALISMO_COSTO     decimal not null,
   primary key (VANDALISMO_ID)
);

alter table EMPLEADO add constraint FK_EMPLEADO_CARGO foreign key (CARGO_ID)
      references CARGO (CARGO_ID) on delete restrict on update restrict;

alter table EMPLEADO add constraint FK_HOTEL_EMPLEADOS foreign key (HOTEL_ID)
      references HOTEL (HOTEL_ID) on delete restrict on update restrict;

alter table FACTURA add constraint FK_CLIENTE_FACTURA foreign key (CLIENTE_ID)
      references CLIENTE (CLIENTE_ID) on delete restrict on update restrict;

alter table FACTURA add constraint FK_FACTURA_DESCUENTO foreign key (DESCUENTO_ID)
      references DESCUENTO (DESCUENTO_ID) on delete restrict on update restrict;

alter table FACTURA add constraint FK_HABITACION_FACTURA foreign key (HABITACION_ID)
      references HABITACION (HABITACION_ID) on delete restrict on update restrict;

alter table FACTURA add constraint FK_HOTEL_FACTURA foreign key (HOTEL_ID)
      references HOTEL (HOTEL_ID) on delete restrict on update restrict;

alter table FACTURA_SERVICIO add constraint FK_FACTURA_SERVICIO foreign key (SERVICIO_ID)
      references SERVICIO (SERVICIO_ID) on delete restrict on update restrict;

alter table FACTURA_SERVICIO add constraint FK_FACTURA_SERVICIO2 foreign key (FACTURA_ID)
      references FACTURA (FACTURA_ID) on delete restrict on update restrict;

alter table FACTURA_VANDALISMO add constraint FK_FACTURA_VANDALISMO foreign key (VANDALISMO_ID)
      references VANDALISMO (VANDALISMO_ID) on delete restrict on update restrict;

alter table FACTURA_VANDALISMO add constraint FK_FACTURA_VANDALISMO2 foreign key (FACTURA_ID)
      references FACTURA (FACTURA_ID) on delete restrict on update restrict;

alter table HABITACION add constraint FK_HABITACION_TIPO foreign key (HT_ID)
      references HABITACION_TIPO (HT_ID) on delete restrict on update restrict;

alter table HABITACION add constraint FK_HOTEL_HABITACION foreign key (HOTEL_ID)
      references HOTEL (HOTEL_ID) on delete restrict on update restrict;

alter table MANTENIMIENTO add constraint FK_HABITACION_MANTENIMIENTO foreign key (HABITACION_ID)
      references HABITACION (HABITACION_ID) on delete restrict on update restrict;

alter table MANTENIMIENTO add constraint FK_MANTENIMIENTO_EMPLEADO foreign key (EMPLEADO_ID)
      references EMPLEADO (EMPLEADO_ID) on delete restrict on update restrict;

alter table MANTENIMIENTO add constraint FK_MANTENIMIENTO_TIPO foreign key (MT_ID)
      references MANTENIMIENTO_TIPO (MT_ID) on delete restrict on update restrict;

alter table SERVICIO add constraint FK_SERVICIO_EMPLEADO foreign key (EMPLEADO_ID)
      references EMPLEADO (EMPLEADO_ID) on delete restrict on update restrict;

