<html>
  <head>
    <title>ETL</title>
    <script src="jquery-3.4.1.min.js"></script>
    <link rel="stylesheet" href="styles.css">

    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css"/>
  	<script type="text/javascript" src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
  </head>
  <body>
    <div class="bg"></div>
    <div class="block">
      <table id="table" class="tables">
        <thead>
          <tr>
            <th>TIEM</th>
            <th>DUR</th>
            <th>H_NUM</th>
            <th>N_MAN</th>
            <th>EMPL</th>
            <th>T_HAB</th>
            <th>HT_DESC</th>
            <th>HB_LOC</th>
            <th>FECHA</th>
            
          </tr>
        </thead>
        <tbody class="listEmployees">
          <?php include_once 'data.php' ?>
        </tbody>
      </table>
      
      <a href="grafica.php">Ver Grafica</a> <br></br>
      <a href="index.php">Regresar</a> 
    </div>
    
    
    <div class="modalFrmEmployee"></div>

      

  </body>
  
  <script src="funcs.js"></script>
</html>