<?php
require_once 'dbconnect/database.class.php';

$objDB = new DataBase;

$result = $objDB->Execute("select IDperiodo, 
periodoTiempo, HABITACION.HABITACION_NUMERO, 
count(MANTENIMIENTO_TIPO.MT_ID) AS NUMERO_MANTENIMIENTO, 
EMPLEADO.EMPLEADO_NOMBRE, MANTENIMIENTO_TIPO.MT_DESCRIPCION AS TIPO_HABITACION, 
habitacion_tipo.HT_DESCRIPCION, habitacion.HABITACION_LOCACION, 
MANTENIMIENTO.MANTENIMIENTO_FECHA 
from periodo, mantenimiento 
inner join HABITACION ON MANTENIMIENTO.HABITACION_ID = HABITACION.HABITACION_ID 
inner join EMPLEADO ON mantenimiento.EMPLEADO_ID = EMPLEADO.EMPLEADO_ID
inner JOIN HABITACION_TIPO ON HABITACION.HT_ID = HABITACION_TIPO.HT_ID 
inner join MANTENIMIENTO_TIPO ON MANTENIMIENTO.MT_ID = MANTENIMIENTO_TIPO.MT_ID
where MANTENIMIENTO_TIPO.MT_DESCRIPCION = 'CAMBIO Y LAVADO DE SABANAS' 
group by HABITACION.HABITACION_NUMERO, MANTENIMIENTO_TIPO.MT_DESCRIPCION;");

while($employee = $result->fetch_assoc()){
    ?>
    <tr>
        <td><?= $employee['IDperiodo'] ?></td>
        <td><?= $employee['periodoTiempo'] ?></td>
        <td><?= $employee['HABITACION_NUMERO'] ?></td>
        <td><?= $employee['EMPLEADO_NOMBRE'] ?></td>
        <td><?= $employee['NUMERO_MANTENIMIENTO'] ?></td>
        <td><?= $employee['TIPO_HABITACION'] ?></td>
        <td><?= $employee['HT_DESCRIPCION'] ?></td>
        <td><?= $employee['HABITACION_LOCACION'] ?></td>
        <td><?= $employee['MANTENIMIENTO_FECHA'] ?></td>
        
    </tr>
    <?php
}
?>