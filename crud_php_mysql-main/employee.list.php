<?php
require_once 'dbconnect/database.class.php';

$objDB = new DataBase;

$result = $objDB->Execute('select 
EMPLEADO_NOMBRE, 
HABITACION_NUMERO,
HABITACION_LOCACION, 
HT_DESCRIPCION, MANTENIMIENTO_TIPO.MT_DESCRIPCION, 
MANTENIMIENTO_FECHA, 
IDperiodo as PERIODO,
periodoTiempo AS DURACION 
from empleado, habitacion, habitacion_tipo,mantenimiento,mantenimiento_tipo,periodo 
ORDER BY `empleado`.`EMPLEADO_NOMBRE` DESC');

while($employee = $result->fetch_assoc()){
  ?>
  <tr>
    <td><?= $employee['EMPLEADO_NOMBRE'] ?></td>
    <td><?= $employee['HABITACION_NUMERO'] ?></td>
    <td><?= $employee['HABITACION_LOCACION'] ?></td>
    <td><?= $employee['HT_DESCRIPCION'] ?></td>
    <td><?= $employee['MT_DESCRIPCION'] ?></td>
    <td><?= $employee['MANTENIMIENTO_FECHA'] ?></td>
    <td><?= $employee['PERIODO'] ?></td>
    <td><?= $employee['DURACION'] ?></td>
    
  </tr>
  <?php
}
?>
