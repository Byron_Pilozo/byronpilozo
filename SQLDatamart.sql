

------------------ consulta para datamart2----------------------
select 
	HABITACION.HABITACION_NUMERO, 
    count(MANTENIMIENTO_TIPO.MT_ID) AS NUMERO_MANTENIMIENTO,
    EMPLEADO.EMPLEADO_NOMBRE, MANTENIMIENTO_TIPO.MT_DESCRIPCION AS TIPO_HABITACION,
    habitacion_tipo.HT_DESCRIPCION,
    habitacion.HABITACION_LOCACION, MANTENIMIENTO.MANTENIMIENTO_FECHA
from mantenimiento
	inner join HABITACION ON MANTENIMIENTO.HABITACION_ID = HABITACION.HABITACION_ID 
    inner join EMPLEADO ON mantenimiento.EMPLEADO_ID = EMPLEADO.EMPLEADO_ID 
    inner JOIN HABITACION_TIPO ON HABITACION.HT_ID = HABITACION_TIPO.HT_ID
	inner join MANTENIMIENTO_TIPO ON MANTENIMIENTO.MT_ID = MANTENIMIENTO_TIPO.MT_ID
	where MANTENIMIENTO_TIPO.MT_DESCRIPCION = 'CAMBIO Y LAVADO DE SABANAS'
	group by HABITACION.HABITACION_NUMERO, MANTENIMIENTO_TIPO.MT_DESCRIPCION;

---------consulta para extraer los datos del tipo de mantenimiento---------------	
	SELECT * FROM mantenimiento_tipo;
	
---------consulta para extraer los datos de habitacion----------
select
habitacion.HABITACION_ID,
HABITACION.HABITACION_NUMERO ,
 habitacion_tipo.HT_DESCRIPCION,
 HABITACION.HABITACION_LOCACION
FROM habitacion
  inner join habitacion_tipo ON (habitacion.HT_ID=habitacion_tipo.HT_ID)


---------consulta para extraer los datos de empleado----------
select
EMPLEADO.EMPLEADO_ID,
EMPLEADO.EMPLEADO_NOMBRE,
CARGO.CARGO_DESCRIPCION
FROM EMPLEADO 
 inner join CARGO ON (EMPLEADO.CARGO_ID=CARGO.CARGO_ID)


---------consulta para extraer los datos de mantenimiento----------
select
MANTENIMIENTO.MANTENIMIENTO_ID,
MANTENIMIENTO_TIPO.MT_ID,
MANTENIMIENTO.MANTENIMIENTO_FECHA
FROM MANTENIMIENTO  
inner join MANTENIMIENTO_TIPO ON (MANTENIMIENTO.MT_ID=MANTENIMIENTO_TIPO.MT_ID)

---------consulta para extraer los datos de los id de las tablas habitacion y empleados para la tabla hechos----------
select
MANTENIMIENTO.MANTENIMIENTO_ID,
EMPLEADO.EMPLEADO_ID,
habitacion.HABITACION_ID
FROM MANTENIMIENTO  
inner join EMPLEADO ON (MANTENIMIENTO.EMPLEADO_ID=EMPLEADO.EMPLEADO_ID)
inner join HABITACION ON (MANTENIMIENTO.HABITACION_ID=HABITACION.HABITACION_ID)


---------consulta para extraer los datos de las nuevas tablas para el datamart----------
    	SELECT
    dim_habitacion.HABITACION_NUMERO, count(dim_mantenimiento.MANTENIMIENTO_ID) as CONTADOR,
    dim_empleado.EMPLEADO_NOMBRE AS EMPLEADO,
    dim_tipo_mantenimiento.MT_DESCRIPCION AS TIPO_MANTENIMIENTO,
    dim_mantenimiento.MANTENIMIENTO_FECHA
    FROM
    hc_servicio_satisfactorio
    INNER JOIN dim_habitacion ON  hc_servicio_satisfactorio.HABITACION_ID = dim_habitacion.HABITACION_ID 
	INNER JOIN dim_mantenimiento ON  hc_servicio_satisfactorio.MANTENIMIENTO_ID = dim_mantenimiento.MANTENIMIENTO_ID
	INNER JOIN dim_empleado ON  hc_servicio_satisfactorio.EMPLEADO_ID = dim_empleado.EMPLEADO_ID 
    INNER JOIN dim_tipo_mantenimiento ON  dim_mantenimiento.MT_ID = dim_tipo_mantenimiento.MT_ID
    where dim_tipo_mantenimiento.MT_DESCRIPCION = 'CAMBIO Y LAVADO DE SABANAS'
    group by dim_tipo_mantenimiento.MT_DESCRIPCION;