/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     03/12/2021 08:20:35 p. m.                    */
/*==============================================================*/


drop table if exists DIM_EMPLEADO;

drop table if exists DIM_HABITACION;

drop table if exists DIM_MANTENIMIENTO;

drop table if exists DIM_TIPO_MANTENIMIENTO;

drop table if exists HC_SERVICIO_SATISFACTORIO;

/*==============================================================*/
/* Table: DIM_EMPLEADO                                          */
/*==============================================================*/
create table DIM_EMPLEADO
(
   EMPLEADO_ID          int not null,
   EMPLEADO_NOMBRE      varchar(100),
   CARGO_DESCRIPCION    varchar(100),
   primary key (EMPLEADO_ID)
);

/*==============================================================*/
/* Table: DIM_HABITACION                                        */
/*==============================================================*/
create table DIM_HABITACION
(
   HABITACION_ID        int not null,
   HABITACION_NUMERO    int,
   HABITACION_LOCACION  varchar(100),
   HT_DESCRIPCION       longtext,
   primary key (HABITACION_ID)
);

/*==============================================================*/
/* Table: DIM_MANTENIMIENTO                                     */
/*==============================================================*/
create table DIM_MANTENIMIENTO
(
   MANTENIMIENTO_ID     int not null,
   MT_ID                decimal,
   MANTENIMIENTO_FECHA  date,
   primary key (MANTENIMIENTO_ID)
);

/*==============================================================*/
/* Table: DIM_TIPO_MANTENIMIENTO                                */
/*==============================================================*/
create table DIM_TIPO_MANTENIMIENTO
(
   MT_ID                decimal not null,
   MT_DESCRIPCION       varchar(100),
   primary key (MT_ID)
);

/*==============================================================*/
/* Table: HC_SERVICIO_SATISFACTORIO                             */
/*==============================================================*/
create table HC_SERVICIO_SATISFACTORIO
(
   MANTENIMIENTO_ID     int not null,
   EMPLEADO_ID          int not null,
   HABITACION_ID        int not null,
   primary key (MANTENIMIENTO_ID, EMPLEADO_ID, HABITACION_ID)
);

alter table DIM_MANTENIMIENTO add constraint FK_DIM_TIPO_MANTENIMIENTO___DIM_MANTENIMIENTO foreign key (MT_ID)
      references DIM_TIPO_MANTENIMIENTO (MT_ID) on delete restrict on update restrict;

alter table HC_SERVICIO_SATISFACTORIO add constraint FK_DIM_EMPLEADO____HC_SERVICIO_SATISFACTORIO foreign key (EMPLEADO_ID)
      references DIM_EMPLEADO (EMPLEADO_ID) on delete restrict on update restrict;

alter table HC_SERVICIO_SATISFACTORIO add constraint FK_DIM_HABITACION____HC_SERVICIO_SATISFACTORIO foreign key (HABITACION_ID)
      references DIM_HABITACION (HABITACION_ID) on delete restrict on update restrict;

alter table HC_SERVICIO_SATISFACTORIO add constraint FK_DIM_MANTENIMIENTO____HC_SERVICIO_SATISFACTORIO foreign key (MANTENIMIENTO_ID)
      references DIM_MANTENIMIENTO (MANTENIMIENTO_ID) on delete restrict on update restrict;

